@echo off
SET makeconfig=%1
SET gccdir=c:\gccarm


IF "%makeconfig%"=="" ( set makeconfig=release)

SET projectincludes=-I.\LPCOpen11xx\Inc\ -I.\LPCProject\Inc
SET projectlib=-L.\LPCOpen11xx\bin\%makeconfig%\
SET defines=-DCORE_M0 -D__LPC11XX__ -DNO_BOARD_LIB -D__USE_LPCOPEN

IF "%makeconfig%"=="debug" (  
    SET opt=-DDEBUG
)ELSE IF "%makeconfig%"=="release" (
    set opt=-O1
)

IF "%makeconfig%"=="clean" ( 

    del .\LPCOpen11xx\bin\Debug\*.* /q
    del .\LPCOpen11xx\bin\Release\*.* /q
    del .\LPCOpen11xx\obj\Debug\*.* /q
    del .\LPCOpen11xx\obj\Release\*.* /q

)ELSE (       
    FOR %%i in (.\LPCOpen11xx\Src\chip_11xx\*.c) DO (
        %gccdir%\bin\arm-none-eabi-gcc.exe -Wall -c -nostartfiles %opt% %defines% -mcpu=cortex-m0 -mthumb -nostdlib -I .\LPCOpen11xx\Inc\ %%i -o .\LPCOpen11xx\obj\%makeconfig%\%%~ni.o -lgcc
        %gccdir%\bin\arm-none-eabi-ar.exe rcs .\LPCOpen11xx\bin\%makeconfig%\libLPCOpen11xx.a  .\LPCOpen11xx\obj\%makeconfig%\%%~ni.o
    )  
    FOR %%i in (.\LPCOpen11xx\Src\chip_common\*.c) DO (
        %gccdir%\bin\arm-none-eabi-gcc.exe -Wall -c -nostartfiles %opt% %defines% -mcpu=cortex-m0 -mthumb -nostdlib -I .\LPCOpen11xx\Inc\ %%i -o .\LPCOpen11xx\obj\%makeconfig%\%%~ni.o -lgcc
        %gccdir%\bin\arm-none-eabi-ar.exe rcs .\LPCOpen11xx\bin\%makeconfig%\libLPCOpen11xx.a  .\LPCOpen11xx\obj\%makeconfig%\%%~ni.o
    )
    
    %gccdir%\bin\arm-none-eabi-gcc.exe -Wall -nostdlib %opt% %defines% -mcpu=cortex-m0 -mthumb %projectincludes% %projectlib% -T .\LPCProject\gcc_arm.ld .\LPCProject\Src\*.* -o .\LPCProject\bin\%makeconfig%\LPCProject.elf -lLPCOpen11xx -lgcc
    %gccdir%\bin\arm-none-eabi-objcopy.exe -O ihex .\LPCProject\bin\%makeconfig%\LPCProject.o .\LPCProject\bin\%makeconfig%\LPCProject.hex
    %gccdir%\bin\arm-none-eabi-size.exe .\LPCProject\bin\%makeconfig%\LPCProject.elf
)